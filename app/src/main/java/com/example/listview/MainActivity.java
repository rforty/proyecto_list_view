package com.example.listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
     private TextView tv1;
     private ListView lv1;

     private String nombres [] ={"Samuel", "Valentina" , "Santiago", " Alejandro", "Valeria",
                   "Gerardo","Carlos", "David", "Sofía"};
     private String edades [] = {"18", "25", "32","17", "24", "20","27", "15", "19","23" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv1= (TextView)findViewById(R.id.tv1);
        lv1= (ListView)findViewById(R.id.lv1);

        //tipo de objeto Array Adapter para que podamos colocar los elemeentos que tenemos
        //asignados en nuestro vector nombres, dentro de nuestro control listview
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item_geekipedia, nombres);
        //metodo setAdapter para mostrar lo que configuramos
        lv1.setAdapter(adapter);
        //EL PARAMETRO DE ESTE METODO VA A SER UNA CLASE ANONIMA
        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                tv1.setText("La edad de " + nombres[position]+ " es " + edades[position] + " años");
            }
        });//

    }
}
